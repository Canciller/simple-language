#include "simple/visitor.h" // expr, stmt, visitor

using namespace simple;
using namespace expr;

#define ACCEPT_VISITOR(T)\
Any T::accept(Visitor& visitor) {\
    return visitor.visit(*this);\
}

Assign::Assign(const Token& name, Expr::SharedPtr value) : name(name), value(value) {}

ACCEPT_VISITOR(Assign)

Binary::Binary(Expr::SharedPtr left, const Token& op, Expr::SharedPtr right) : left(left),
                                                                               op(op),
                                                                               right(right) {}
ACCEPT_VISITOR(Binary) 
Grouping::Grouping(Expr::SharedPtr ex) : ex(ex) {}

ACCEPT_VISITOR(Grouping)

Literal::Literal(const Any& value) : value(value) {}

ACCEPT_VISITOR(Literal)

Uninary::Uninary(const Token& op, Expr::SharedPtr right) : op(op), 
                                                           right(right) {}

ACCEPT_VISITOR(Uninary)

Variable::Variable(const Token& name) : name(name) {}

ACCEPT_VISITOR(Variable)

Logical::Logical(Expr::SharedPtr left, const Token& op, Expr::SharedPtr right) : left(left),
                                                                                 op(op),
                                                                                 right(right) {}

ACCEPT_VISITOR(Logical)

Conditional::Conditional(Expr::SharedPtr condition, Expr::SharedPtr left, Expr::SharedPtr right) : condition(condition),
                                                                                                   left(left),
                                                                                                   right(right) {}

ACCEPT_VISITOR(Conditional)

Call::Call(Expr::SharedPtr callee, const Token& paren, const ExprList& arguments) : callee(callee), 
                                                                                    paren(paren),
                                                                                    arguments(arguments) {}

ACCEPT_VISITOR(Call)
