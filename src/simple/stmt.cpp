#include "simple/visitor.h" // stmt, expr, vistor

using namespace simple;
using namespace stmt;

#define ACCEPT_VISITOR(T)\
void T::accept(Visitor& visitor) {\
    return visitor.visit(*this);\
}

Block::Block(const StmtList& statements) : statements(statements) {}

ACCEPT_VISITOR(Block)

Expression::Expression(ExprSharedPtr ex) : ex(ex) {}

ACCEPT_VISITOR(Expression)

Print::Print(ExprSharedPtr ex) : ex(ex) {}

ACCEPT_VISITOR(Print)

Var::Var(const Token& name, ExprSharedPtr initializer) : name(name), initializer(initializer) {}

ACCEPT_VISITOR(Var)

If::If(ExprSharedPtr condition, StmtSharedPtr ifBranch, StmtSharedPtr elseBranch) : condition(condition),
                                                                                    ifBranch(ifBranch),
                                                                                    elseBranch(elseBranch) {}

ACCEPT_VISITOR(If)                                                                                

While::While(ExprSharedPtr condition, StmtSharedPtr body) : condition(condition), body(body) {}

ACCEPT_VISITOR(While)

Keyword::Keyword(const Token& keyword) : keyword(keyword) {}

ACCEPT_VISITOR(Keyword)

Function::Function(const Token& name, const TokenList& params, const StmtList& body) : name(name), params(params), body(body) {}

ACCEPT_VISITOR(Function)
