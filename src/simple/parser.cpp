#include "simple/parser.h"
#include "simple/error.h"
#include "simple/flags.h"
#include "simple/printers/ast_printer.h"

using namespace simple;
using namespace expr;
using namespace stmt; 
Parser::Parser() : mCurrent(0), mError(false) {}

Parser::Parser(const Parser& other)
{ 
    mTokens = other.mTokens;
    mCurrent = other.mCurrent;
}
Parser::Parser(Parser&& other) : Parser()
{
    swap(*this, other);
}

Parser& Parser::operator=(Parser other)
{
    swap(*this, other); 
    return *this;
}

void simple::swap(Parser& first, Parser& second)
{
    using std::swap;
    swap(first.mTokens, second.mTokens);
    swap(first.mCurrent, second.mCurrent);
}

Parser::Parser(TokenList tokens)
{
    init(tokens);
}

void Parser::reset()
{
    mTokens.clear();
    mCurrent = 0;
    mError = false;
}

void Parser::init(TokenList tokens) 
{
    mTokens = tokens;
    mCurrent = 0;
    mError = false;
}

StmtList Parser::parse()
{
    try {
        StmtList statements;
        while(!isAtEnd()) {
            Stmt::SharedPtr stmt = declaration();
            if(stmt) statements.push_back(stmt);
        }
        return statements;
    } catch(ParseError& e) {
        parseError(e);
        return StmtList();
    }
}

void Parser::parseError(const ParseError& e)
{
    mError = true;
    Status::singleton()->report(e.token(), "ParseError: " + std::string(e.what()));
}

bool Parser::hasError() const
{
    return mError;
}

Stmt::SharedPtr Parser::declaration()
{
    try {
        if(match({TOKEN_VAR})) 
            return varDeclaration(); 
        return statement();
    } catch(ParseError& e) {
        synchronize(); 
        throw e;
        return Stmt::SharedPtr();
    }
}

Stmt::SharedPtr Parser::varDeclaration()
{
    Token name = consume(TOKEN_IDENTIFIER, "Expected variable name");

    Expr::SharedPtr initializer;
    if(match({TOKEN_EQUAL}))
        initializer = expression();

    consume(TOKEN_SEMICOLON, "Expected ';' after variable declaration");
    return Stmt::SharedPtr(new Var(name, initializer));
}

Stmt::SharedPtr Parser::statement()
{
    if(match({TOKEN_FUNCTION}))
        return function("function");
    if(match({TOKEN_IF}))
        return ifStatement();
    if(match({TOKEN_WHILE}))
        return whileStatement();
    if(match({TOKEN_FOR}))
        return forStatement();
    if(match({TOKEN_PRINT})) 
        return printStatement();
    if(match({TOKEN_LEFT_BRACE})) 
        return Stmt::SharedPtr(new Block(block()));
    if(match({TOKEN_BREAK, TOKEN_CONTINUE})) {
        const Token& keyword = previous();
        consume(TOKEN_SEMICOLON, "Expected ';' after '" + keyword.lexeme() + "'");
        return Stmt::SharedPtr(new stmt::Keyword(keyword));
    }
    return expressionStatement();
}

Stmt::SharedPtr Parser::function(const std::string& type)
{
    const Token& name = consume(TOKEN_IDENTIFIER, "Expected " + type + " name");
    consume(TOKEN_LEFT_PAREN, "Expected '(' after " + type + " name");
    TokenList params;
    if(!check(TOKEN_RIGHT_PAREN)) {
        do {
            if(params.size() >= 32) 
                parseError(ParseError(peek(), "Cannot have more than 32 parameters"));
            params.push_back(consume(TOKEN_IDENTIFIER, "Expected parameter name"));
        } while(match({TOKEN_COMMA}));
    }
    consume(TOKEN_RIGHT_PAREN, "Expected ')' after " + type + " name");
    consume(TOKEN_LEFT_BRACE, "Expected '{' before " + type + " body");
    const StmtList& body = block();
    return Stmt::SharedPtr(new Function(name, params, body));
}

StmtList Parser::block()
{
    StmtList statements;
    
    while(!check(TOKEN_RIGHT_BRACE) && !isAtEnd())
        statements.push_back(declaration());

    consume(TOKEN_RIGHT_BRACE, "Expected '}' after block");
    return statements;
}

Stmt::SharedPtr Parser::printStatement()
{
    Expr::SharedPtr value = expression();
    consume(TOKEN_SEMICOLON, "Expected ';' after expression");
    return Stmt::SharedPtr(new stmt::Print(value));
}

Stmt::SharedPtr Parser::expressionStatement()
{
    Expr::SharedPtr value = expression();
    if(Flags::singleton()->interactive && peek().type() == TOKEN_EOF) {
        ASTPrinter printer;
        std::cout << printer.print(*value) << '\n';
        return Stmt::SharedPtr(new stmt::Print(value));
    }
    
    consume(TOKEN_SEMICOLON, "Expected ';' after expression");
    return Stmt::SharedPtr(new stmt::Expression(value));
}

Stmt::SharedPtr Parser::ifStatement()
{
    consume(TOKEN_LEFT_PAREN, "Expected '(' after if");
    Expr::SharedPtr condition = expression();
    consume(TOKEN_RIGHT_PAREN, "Expected ')' after if condition");
    
    Stmt::SharedPtr ifBranch = statement();
    Stmt::SharedPtr elseBranch;
    if(match({TOKEN_ELSE}))
        elseBranch = statement();

    return Stmt::SharedPtr(new stmt::If(condition, ifBranch, elseBranch));
}

Stmt::SharedPtr Parser::whileStatement()
{
    consume(TOKEN_LEFT_PAREN, "Expected '(' after while");
    Expr::SharedPtr condition = expression();
    consume(TOKEN_RIGHT_PAREN, "Expected ')' after condition");

    Stmt::SharedPtr body = statement();

    return Stmt::SharedPtr(new stmt::While(condition, body));
}

Stmt::SharedPtr Parser::forStatement()
{
    consume(TOKEN_LEFT_PAREN, "Expected '(' after for");

    Stmt::SharedPtr initializer;
    if(match({TOKEN_SEMICOLON}));
    else if(match({TOKEN_VAR})) initializer = varDeclaration();
    else initializer = expressionStatement();
    
    Expr::SharedPtr condition;
    if(!check(TOKEN_SEMICOLON))
        condition = expression();
    consume(TOKEN_SEMICOLON, "Expected ';' after loop condition");

    Expr::SharedPtr increment;
    if(!check(TOKEN_RIGHT_PAREN))
        increment = expression();
    consume(TOKEN_RIGHT_PAREN, "Expected ')' after for clauses");
    
    Stmt::SharedPtr body = statement();
     
    if(increment) {
        body = Stmt::SharedPtr(new stmt::Block(StmtList({
            body,
            Stmt::SharedPtr(new stmt::Expression(increment))
        })));
    }

    if(!condition) condition = ExprSharedPtr(new expr::Literal(true));
    
    body = Stmt::SharedPtr(new stmt::While(condition, body));
    
    if(initializer) {
        body = Stmt::SharedPtr(new stmt::Block(StmtList({
            initializer,
            body
        })));
    }

    return body;
}

Expr::SharedPtr Parser::expression() 
{
    return assigment();
}

Expr::SharedPtr Parser::assigment()
{
    auto ex = conditional();

    if(match({TOKEN_EQUAL})) {
        Token equals = previous();
        auto value = assigment();

        if(typeid(*ex) == typeid(Variable)) {
            Token name = std::static_pointer_cast<Variable>(ex)->name; 
            return Expr::SharedPtr(new Assign(name, value));
        }
        
        throw ParseError(equals, "Invalid assignment target");
    }

    return ex;
}

Expr::SharedPtr Parser::conditional()
{
    auto ex = logicalOr();  

    while(match({TOKEN_QUESTION})) {
        auto left = expression();
        consume(TOKEN_COLON, "Expected ':' after condition"); 
        auto right = expression();
        ex = Expr::SharedPtr(new Conditional(ex, left, right));
    }

    return ex;
}

Expr::SharedPtr Parser::logicalOr()
{
    auto ex = logicalAnd();

    while(match({TOKEN_OR})) {
        const Token& op = previous();
        auto right = logicalAnd(); 
        ex =  Expr::SharedPtr(new Logical(ex, op, right));
    }

    return ex;
}

Expr::SharedPtr Parser::logicalAnd()
{
    auto ex = equality();

    while(match({TOKEN_AND})) {
        const Token& op = previous();
        auto right = equality();
        ex =  Expr::SharedPtr(new Logical(ex, op, right));
    }

    return ex;
}

Expr::SharedPtr Parser::equality() 
{
    auto ex = comparison();
    
    while(match({TOKEN_BANG_EQUAL, TOKEN_EQUAL_EQUAL})) {
        auto op = previous();
        auto right = comparison();
        ex = Expr::SharedPtr(new Binary(ex, op, right));
    }

    return ex;
}

Expr::SharedPtr Parser::comparison()
{
    auto ex = addition();

    while(match({TOKEN_GREATER, TOKEN_GREATER_EQUAL, TOKEN_LESS, TOKEN_LESS_EQUAL})) {
        auto op = previous();
        auto right = addition();
        ex = Expr::SharedPtr(new Binary(ex, op, right));
    }

    return ex;
}

Expr::SharedPtr Parser::addition()
{
    auto ex = multiplication();
    
    while(match({TOKEN_PLUS, TOKEN_MINUS})) {
        auto op = previous();
        auto right = multiplication();
        ex = Expr::SharedPtr(new Binary(ex, op, right));
    }

    return ex;
}

Expr::SharedPtr Parser::multiplication()
{
    auto ex = uninary();

    while(match({TOKEN_STAR, TOKEN_SLASH})) {
        auto op = previous(); 
        auto right = uninary();
        ex = Expr::SharedPtr(new Binary(ex, op, right));
    }

    return ex;
}

Expr::SharedPtr Parser::uninary()
{
    if(match({TOKEN_BANG, TOKEN_MINUS, TOKEN_PLUS})) {
        auto op = previous();
        auto right = uninary();
        return Expr::SharedPtr(new Uninary(op, right));
    }

    return call();
}

Expr::SharedPtr Parser::call()
{
    auto ex = primary(); 
    while(true) {   
        if(match({TOKEN_LEFT_PAREN}))
            ex = finishCall(ex);
        else break;
    }

    return ex;
}

Expr::SharedPtr Parser::finishCall(Expr::SharedPtr callee)
{
    ExprList arguments;    
    if(!check(TOKEN_RIGHT_PAREN)) {
        do {
            if(arguments.size() >= 32)
                parseError(ParseError(peek(), "Cannot have more than 32 arguments"));
            arguments.push_back(expression()); 
        } while(match({TOKEN_COMMA}));
    }

    Token paren = consume(TOKEN_RIGHT_PAREN, "Expected ')' after arguments");
    
    return Expr::SharedPtr(new Call(callee, paren, arguments));
}

Expr::SharedPtr Parser::primary()
{
    if(match({TOKEN_FALSE, TOKEN_TRUE, TOKEN_NIL, TOKEN_NUMBER, TOKEN_STRING}))
        return Expr::SharedPtr(new Literal(previous().value()));
    
    if(match({TOKEN_IDENTIFIER}))
        return Expr::SharedPtr(new Variable(previous()));

    if(match({TOKEN_LEFT_PAREN})) {
        auto ex = expression();
        consume(TOKEN_RIGHT_PAREN, "Expected ')' after expression");
        return Expr::SharedPtr(new Grouping(ex));
    }

    throw ParseError(peek(), "Expected expression");
}

const Token& Parser::consume(TokenType type, const std::string& message)
{
    if(peek().type() == TOKEN_EOF &&
       Flags::singleton()->interactive && 
       type == TOKEN_SEMICOLON) return advance();

    if(check(type)) return advance();
    throw ParseError(peek(), message);
}

bool Parser::match(const std::initializer_list<TokenType>& types) 
{
    for(TokenType type : types) {
        if(check(type)) {
            advance();
            return true;
        }
    }

    return false;
}

bool Parser::check(TokenType type)
{
    if(isAtEnd()) return false;
    return peek().type() == type;
}

Token& Parser::advance()
{
    if(!isAtEnd()) ++mCurrent;
    return previous();
}

Token& Parser::peek()
{
    return mTokens[mCurrent];
}

Token& Parser::previous()
{
    return mTokens[mCurrent - 1];
}

bool Parser::isAtEnd()
{
    return peek().type() == TOKEN_EOF;
}

void Parser::synchronize() {
    advance();
    
    while(!isAtEnd()) {
        if(previous().type() == TOKEN_SEMICOLON) return;
        
        switch(peek().type()) {
            case TOKEN_CLASS:
            case TOKEN_FUNCTION:
            case TOKEN_VAR:
            case TOKEN_FOR:
            case TOKEN_IF:
            case TOKEN_WHILE:
            case TOKEN_PRINT:
            case TOKEN_RETURN:
            default: // TODO Remove this later.
                return;
        }

        advance();
    }
}
