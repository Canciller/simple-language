#include "simple/interpreter.h"
#include "simple/function.h"

using namespace simple;

class JumpStatement
{
public:
    JumpStatement(TokenType type) : type(type) {}

    TokenType type;
};

Interpreter::Interpreter() : mEnvironment(nullptr), mGlobal(nullptr), mError(false) 
{
    mGlobal = new Enviroment(); 
    mGlobal->define("greet", CallableSharedPtr(new Greet()));
    mEnvironment = mGlobal;
}

Interpreter::~Interpreter()
{
    if(mEnvironment)
        delete mEnvironment;
    mGlobal = nullptr;
    mEnvironment = nullptr;
}

void Interpreter::reset()
{
    mError = false;
}

Enviroment* Interpreter::globals()
{
    return mGlobal;
}

void Interpreter::interpret(StmtList& statements)
{
    try {
        for(auto& statement : statements) { if(statement) execute(statement);
        }
    } catch(RuntimeError& e) {
        runtimeError(e); 
    } catch(JumpStatement& s) {}
    
    mEnvironment = mGlobal;
}

void Interpreter::runtimeError(RuntimeError& e)
{
    mError = true;
    Status::singleton()->report(e.token(), "RuntimeError: " + std::string(e.what()));
}

bool Interpreter::hasError() const
{
    return mError;
}

Any Interpreter::visit(expr::Assign& expr) // expr::Assign
{
    Any value = evaluate(expr.value);
    mEnvironment->assign(expr.name, value);

    return value;
}

Any Interpreter::visit(expr::Binary& expr) // expr::Binary
{
    Any l = evaluate(expr.left), r = evaluate(expr.right);
    TokenType type = expr.op.type();
    
    switch(type) {
        case TOKEN_PLUS:
            if(TYPE_STR(l) || TYPE_STR(r))
                return l.toString() + r.toString();
            checkNumberOperands(expr.op, l, r);
            if(TYPE_NUM(l) && TYPE_NUM(r))
                return TO_NUM(l) + TO_NUM(r);
        case TOKEN_MINUS: 
            checkNumberOperands(expr.op, l, r);
            return TO_NUM(l) - TO_NUM(r);

        case TOKEN_STAR:
            checkNumberOperands(expr.op, l, r);
            return TO_NUM(l) * TO_NUM(r);
        case TOKEN_SLASH: {
            checkNumberOperands(expr.op, l, r);
            double n; if((n = TO_NUM(r)) == 0.0)
                throw RuntimeError(expr.op, "Cannot divide by zero");
            return TO_NUM(l) / n;
        } 

        case TOKEN_GREATER:
            checkNumberOperands(expr.op, l, r);
            return TO_NUM(l) > TO_NUM(r);
        case TOKEN_GREATER_EQUAL:
            checkNumberOperands(expr.op, l, r);
            return TO_NUM(l) >= TO_NUM(r);
        case TOKEN_LESS:
            checkNumberOperands(expr.op, l, r);
            return TO_NUM(l) < TO_NUM(r);
        case TOKEN_LESS_EQUAL:
            checkNumberOperands(expr.op, l, r);
            return TO_NUM(l) <= TO_NUM(r);
        
        case TOKEN_EQUAL_EQUAL:
            return isEqual(l, r);
        case TOKEN_BANG_EQUAL:
            return !isEqual(l, r);

        default: break;
    }

    throw RuntimeError(expr.op, "Invalid binary operator");
}

Any Interpreter::visit(expr::Grouping& expr) // expr::Grouping
{
    return evaluate(expr.ex);
}

Any Interpreter::visit(expr::Uninary& expr) // expr::Uninary
{
    Any right = evaluate(expr.right);
    TokenType type = expr.op.type();
    switch(type) {
        case TOKEN_BANG:
            return !isTruthy(right);        
        case TOKEN_MINUS:
        case TOKEN_PLUS:
            if(!TYPE_NUM(right))
                throw RuntimeError(expr.op, "Operand must be a number");
            return type == TOKEN_MINUS ? -TO_NUM(right) : TO_NUM(right);
        default: break;
    }

    throw RuntimeError(expr.op, "Invalid uninary operator");
}

Any Interpreter::visit(expr::Literal& expr) // expr::Literal
{
    return expr.value;
} 

Any Interpreter::visit(expr::Variable& expr) // expr::Variable
{
    return mEnvironment->get(expr.name);
}

Any Interpreter::visit(expr::Logical& expr) // expr::Logical
{
    Any left = evaluate(expr.left);
    
    bool truthy = isTruthy(left);
    if(expr.op.type() == TOKEN_OR) {
        if(truthy) return left;
    } else {
       if(!truthy) return left; 
    }

    return evaluate(expr.right);
}

Any Interpreter::visit(expr::Conditional& expr) // expr::Conditional
{
    Any condition = evaluate(expr.condition);
    if(isTruthy(condition))
        return evaluate(expr.left);
    
    return evaluate(expr.right);
}

Any Interpreter::visit(expr::Call& expr) // expr::Call
{
    Any callee = evaluate(expr.callee);  
    
    Callable::ArgList arguments;
    for(auto& argc : expr.arguments) 
        arguments.push_back(evaluate(argc));
    
    if(!callee.isType<CallableSharedPtr>())
        throw RuntimeError(expr.paren, "Can only call functions and classes");

    CallableSharedPtr fun = callee.castTo<CallableSharedPtr>();

    if(fun->ariaty() != arguments.size()) {
        std::stringstream ss;
        ss << "Expected " << fun->ariaty() << " arguments but got " << arguments.size();
        throw RuntimeError(expr.paren, ss.str());
    }
        
    return fun->call(*this, arguments);
}

void Interpreter::visit(stmt::Block& stmt) // stmt::Block
{
    Enviroment environment(mEnvironment);
    executeBlock(stmt.statements, &environment); 
}

void Interpreter::visit(stmt::Expression& expr) // stmt::Expression
{
    evaluate(expr.ex); 
}

void Interpreter::visit(stmt::Print& stmt) // stmt::Print
{
    Any value = evaluate(stmt.ex); 
    std::cout << (value.empty() ? "nil" : value.toString()) << '\n';
}

void Interpreter::visit(stmt::Var& stmt) // stmt::Var
{
    Any value;
    if(stmt.initializer) value = evaluate(stmt.initializer);
    
    mEnvironment->define(stmt.name.lexeme(), value);
}

void Interpreter::visit(stmt::If& stmt) // stmt::If
{
    Any condition = evaluate(stmt.condition);
    if(isTruthy(condition))
        execute(stmt.ifBranch);
    else if(stmt.elseBranch)
        execute(stmt.elseBranch);
}

void Interpreter::visit(stmt::While& stmt) // stmt::While
{
    while(isTruthy(evaluate(stmt.condition))) {
        try {
            execute(stmt.body);
        } catch(JumpStatement& s) {
            if(s.type == TOKEN_BREAK) break;
        } 
    }
}

void Interpreter::visit(stmt::Keyword& stmt) // stmt::Keyword
{
    switch(stmt.keyword.type()) {
        case TOKEN_BREAK:
            throw JumpStatement(TOKEN_BREAK);
        case TOKEN_CONTINUE:
            throw JumpStatement(TOKEN_CONTINUE);
            return;
        default: break;
    }

    throw RuntimeError(stmt.keyword, "'"+ stmt.keyword.lexeme() + "' is a reserved keyword");
}

void Interpreter::visit(stmt::Function& stmt) // stmt::Function
{
    CallableSharedPtr fun = CallableSharedPtr(new Function(stmt));
    mEnvironment->define(stmt.name.lexeme(), fun);
}

void Interpreter::execute(stmt::Stmt::SharedPtr& stmt)
{
    stmt->accept(*this);
}

void Interpreter::executeBlock(StmtList& statements, Enviroment* environment)
{
    Enviroment* previous = mEnvironment; 
    mEnvironment = environment;
    try {
        for(auto& statement : statements)
            execute(statement);
    } catch(RuntimeError& e) {
        mEnvironment = previous;
        throw e;
    } catch(JumpStatement& s) {
       mEnvironment = previous;
       throw s;
    }
    mEnvironment = previous;
}

Any Interpreter::evaluate(expr::Expr::SharedPtr expr)
{
    return expr->accept(*this);
}

bool Interpreter::isTruthy(const Any& value) const
{
    if(TYPE_NIL(value)) return false;
    if(TYPE_BOOL(value)) return TO_BOOL(value);
    return true;
}

bool Interpreter::isEqual(const Any& a, const Any& b) const
{
    if(TYPE_NIL(a) && TYPE_NIL(b))
        return true;
    if(TYPE_NIL(a) || TYPE_NIL(b))
        return false;
    if(TYPE_BOOL(a) && TYPE_BOOL(b)) 
        return TO_BOOL(a) == TO_BOOL(b);
    if(TYPE_STR(a) && TYPE_STR(b))
        return TO_STR(a) == TO_STR(b);
    if(TYPE_NUM(a) && TYPE_NUM(b))
        return TO_NUM(a) == TO_NUM(b);
        
    return false;
}

void Interpreter::checkStringOperands(const Token& op ,const Any& a, const Any& b) {
    if(TYPE_STR(a) && TYPE_STR(b)) return;
    throw RuntimeError(op, "operands must be strings");
}

void Interpreter::checkNumberOperands(const Token& op ,const Any& a, const Any& b) {
    if(TYPE_NUM(a) && TYPE_NUM(b)) return;
    throw RuntimeError(op, "operands must be numbers");
}
