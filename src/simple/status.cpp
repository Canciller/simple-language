#include "simple/status.h"

using namespace simple;

Status* Status::mInstance = nullptr;

Status::Status() : mSource(nullptr) {}

Status::~Status() 
{
    mSource = nullptr;
}

Status* Status::singleton() 
{
    if(!mInstance)
        mInstance = new Status;
    return mInstance;
}

void Status::destroy()
{
    if(mInstance) delete mInstance;
    mInstance = nullptr;
}

void Status::attachSource(const Source* source)
{
    mSource = source;
}

void Status::report(size_t line, size_t character, 
                    const std::string& message,
                    const std::string& where) const
{
    std::stringstream ss;
    ss << line + 1 << ':' << character + 1 << ' ' << message << '.' << '\n';
    ss << createWhereMessage(line, character, where);
    std::cerr << ss.str();
}

void Status::report(const Token& token, 
                    const std::string& message, 
                    const std::string& where) const
{
    report(token.line(), token.character(), message, where);
}

void Status::report(const Token& token,
                    const std::string& message) const
{
    report(token.line(), token.character(), message, getWhereMessageFromSource(token.line()));
}

void Status::report(const std::string& message)
{ 
    std::cerr << message << '.';
}

std::string Status::createWhereMessage(size_t line, size_t character, const std::string& where) const
{
    if(where.empty()) return where;

    std::stringstream ss, l;
    l << std::string(4, ' ') << line + 1 << " | ";
    std::string lStr = l.str();
    ss << lStr << where << '\n' << std::string(lStr.size() + character, ' ') << "^ Here\n";
    return ss.str();
}

std::string Status::getWhereMessageFromSource(size_t line) const
{
    std::string where;
    if(mSource) where = mSource->getLine(line);
    return where;
}
