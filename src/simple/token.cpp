#include "simple/token.h"

using namespace simple;

struct tt_str_t {
    TokenType type;
    const char* str;
};

tt_str_t tt_str[] = {
    TOKEN_LEFT_PAREN, "LEFT_PAREN",
    TOKEN_RIGHT_PAREN, "RIGHT_PAREN",
    TOKEN_LEFT_BRACE, "LEFT_BRACE",
    TOKEN_RIGHT_BRACE, "RIGHT_BRACE",
    TOKEN_COMMA, "COMMA",
    TOKEN_DOT, "DOT",
    TOKEN_MINUS, "MINUS",
    TOKEN_PLUS, "PLUS",
    TOKEN_SEMICOLON, "SEMICOLON",
    TOKEN_COLON, "COLON",
    TOKEN_SLASH, "SLASH",
    TOKEN_STAR, "STAR",
    TOKEN_QUESTION, "QUESTION",
    TOKEN_BANG, "BANG",
    TOKEN_BANG_EQUAL, "BANG_EQUAL",
    TOKEN_EQUAL, "EQUAL",
    TOKEN_EQUAL_EQUAL, "EQUAL_EQUAL",
    TOKEN_GREATER, "GREATER_EQUAL",
    TOKEN_GREATER_EQUAL, "GREATER_EQUAL",
    TOKEN_LESS, "LESS",
    TOKEN_LESS_EQUAL, "LESS_EQUAL", 
    TOKEN_IDENTIFIER, "IDENTIFIER",
    TOKEN_STRING, "STRING",
    TOKEN_NUMBER, "NUMBER",
    TOKEN_AND, "AND",
    TOKEN_CLASS, "CLASS",
    TOKEN_ELSE, "ELSE",
    TOKEN_FALSE, "FALSE",
    TOKEN_FUNCTION, "FUNCTION",
    TOKEN_FOR, "FOR",
    TOKEN_IF, "IF",
    TOKEN_NIL, "NIL", 
    TOKEN_OR, "OR", 
    TOKEN_PRINT, "PRINT", 
    TOKEN_RETURN, "RETURN",
    TOKEN_SUPER, "SUPER",
    TOKEN_THIS, "THIS",
    TOKEN_TRUE, "TRUE",
    TOKEN_VAR, "VAR",
    TOKEN_WHILE, "WHILE",
    TOKEN_BREAK, "BREAK",
    TOKEN_CONTINUE, "CONTINUE",
    TOKEN_EOF, "EOF",
};

Token::Token() : mType(TOKEN_UNKNOWN), mLine(0), mCharacter(0) {}

Token::Token(TokenType type,
        size_t line,
        size_t character) 
        : mType(type), 
          mLine(line), 
          mCharacter(character) {}

Token::Token(TokenType type, 
             std::string lexeme, 
             std::string literal, 
             size_t line, 
             size_t character) 
             : mType(type), 
               mLexeme(lexeme), 
               mLine(line), 
               mCharacter(character) 
{
    switch(type) {
        case TOKEN_TRUE:
        case TOKEN_FALSE:
            mValue = Any(literal == "true");
            break;
        case TOKEN_STRING:
            mValue = Any(literal);
            break;
        case TOKEN_NUMBER:
            mValue = Any(std::stod(literal));
            break;
        case TOKEN_NIL: break;
        default: break;
    }
}

std::string Token::lexeme() const 
{
    return mLexeme;
}

std::string Token::literal() const
{
    const std::string& s = mValue.toString();
    if(s == "nil") return std::string();
    return s;
}

const Any& Token::value() const
{
    return mValue;    
}

TokenType Token::type() const
{
    return mType;
}

size_t Token::line() const
{
    return mLine;
}

size_t Token::character() const
{
    return mCharacter;
}

std::string Token::toString() const
{
    std::stringstream ss;
    ss << mLine + 1 << ':' << mCharacter + 1 << ' ' << typeString() << ' ' << mLexeme << ' ' << literal();
    return ss.str();
}

const char* Token::typeString() const
{
    size_t size = sizeof tt_str / sizeof *tt_str;
    for(size_t i = 0; i < size; ++i) {
        if(tt_str[i].type == mType)
            return tt_str[i].str;
    }
    return "UNKNOWN";
}
