#include <fstream>
#include <iostream>
#include <string>

#include "simple/flags.h"
#include "simple/simple.h"

using namespace simple;
using namespace expr;

Simple::Simple() : mStatus(Status::singleton()), 
                   mScanner(new Scanner()),
                   mParser(new Parser()),
                   mInterpreter(new Interpreter()),
                   mExitCode(0) {}

Simple::~Simple() 
{
    mStatus = nullptr;
    if(mScanner) delete mScanner;
    if(mParser) delete mParser;
    if(mInterpreter) delete mInterpreter;
    mScanner = nullptr;
    mParser = nullptr;
    mInterpreter = nullptr;
    Status::destroy(); // It is safe to destroy the Status singleton here.
    Flags::destroy();
}

int Simple::run(int argc, char **argv) 
{
    mExitCode = 1;
    if(!argv) return mExitCode;
    if(argc > 2) {
        if(argc > 0) fprintf(stderr, "usage: %s [file]\n", argv[0]);
        return mExitCode;
    } 
    
    mExitCode = 0;
    if(argc == 2) mExitCode = runFile(std::string(argv[1]));
    else runPrompt(); 
    
    return mExitCode;
}

inline int Simple::exitCode() const
{
    return mExitCode;
}

int Simple::runFile(const std::string& path) 
{ 
    std::string source, line;
    std::ifstream file;
    file.open(path);
    if(file.is_open()) {
        while(file.good() && std::getline(file, line)) {
            source += line;
            if(file.peek() != EOF)
                source += '\n';
        }
        if(!file.eof()) return 1;
        file.close();
        
        return process(source);
    } else {
        fprintf(stderr, "%s couldn't be opened for reading.\n", path.c_str());
        return 1;
    }

    return 0;
}

void Simple::runPrompt()
{
    Flags::singleton()->interactive = true; 
    for(;;) {
        std::cout << "> ";
        std::string line;
        std::getline(std::cin, line);
        std::cout << process(line) << ' ';
    }
}

int Simple::process(const std::string& sourceStr)
{  
    mScanner->init(sourceStr);
    const TokenList& tokens = mScanner->scanTokens();
    
    const Source* source = &mScanner->getSource();
    Status::singleton()->attachSource(source);

/*
    for(auto& token : tokens)
        std::cout << token.toString() << '\n'; 
*/

    if(mScanner->hasError()) return 60;

    mParser->init(tokens);
    StmtList statements = mParser->parse();

    if(mParser->hasError()) return 65;
    
    mInterpreter->reset();
    mInterpreter->interpret(statements);

    if(mInterpreter->hasError()) return 70;

    return 0;
}
