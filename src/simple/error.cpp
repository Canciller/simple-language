#include "simple/error.h"
#include "simple/status.h"

using namespace simple;

Error::Error(const Token& token, 
             const std::string& what) : runtime_error(what), mToken(token) {}

Token Error::token() const
{
    return mToken;
}
