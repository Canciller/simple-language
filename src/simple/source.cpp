#include "simple/source.h"

using namespace simple;

Source::Source() {}

Source::Source(const Source& other)
{
    mSource = other.mSource;
    mSourceOffsets = other.mSourceOffsets;
    mCurrentLine = other.mCurrentLine;
    mLineOffsetList = other.mLineOffsetList;
}

Source::Source(Source&& other) : Source()
{
    swap(*this, other);
}

Source& Source::operator=(Source other)
{
    swap(*this, other);
    return *this;
}

void simple::swap(Source& first, Source& second)
{
    using std::swap;
    swap(first.mSource, second.mSource);
    swap(first.mLineOffsetList, second.mLineOffsetList);

    auto sourceOffsets = first.mSourceOffsets; 
    first.mSourceOffsets = second.mSourceOffsets;
    second.mSourceOffsets = sourceOffsets;

    auto currentLine = first.mCurrentLine;
    first.mCurrentLine = second.mCurrentLine;
    second.mCurrentLine = currentLine;
}

Source::Source(const std::string& source) : mSource(source)
{
    saveCurretLineEndIndex();
    saveCurrentLineOffsets();
}

void Source::load(const std::string& source)
{
    reset();
    setSourceString(source);
    saveCurretLineEndIndex();
    saveCurrentLineOffsets();
}

void Source::reset() 
{
    mLineOffsetList.clear();
    clearSourceOffsets();
    clearCurrentLine();
}

// returns the current character been analized and advances the line offsets.
char Source::advance()
{
    mCurrentLine.currentRelative = (++mCurrentLine.nextRelative) - 1;
    mSourceOffsets.current = (++mSourceOffsets.next) - 1;
    return current();
}

size_t Source::nextIndex() const
{
    return mSourceOffsets.next;
}

size_t Source::currentIndex() const
{
    return mSourceOffsets.current;
}

size_t Source::currentIndexRelative() const
{
    return mCurrentLine.currentRelative;
}

size_t Source::nextIndexRelative() const
{
    return mCurrentLine.nextRelative;
}

size_t Source::line() const
{
    return mCurrentLine.lineNumber;
}

// returns the the next character after current without advancing the line offsets.
char Source::peek() const
{
    if(nextAtEnd()) return '\0';
    return mSource[mSourceOffsets.next];
}

// returns the current character without advancing the line offsets.
char Source::current() const
{
    return mSource[mSourceOffsets.current];
}

// returns substring of lines from index start to index end.
std::string Source::get(size_t start, size_t end) const
{
    if(start == 0) { 
        if(end >= length())
            return mSource;
    }

    if(end != std::string::npos) {
        if(end < start)
            return std::string();
        end = end - start + 1;
    }   

    return mSource.substr(start, end);
}

std::string Source::getCurrentLine(bool withLineEnding) const
{
    return get(mCurrentLine.startGlobal, mCurrentLine.endGlobal - (withLineEnding ? 0 : 1));
}

std::string Source::getLine(size_t line, bool withLineEnding) const
{
    size_t actualLine = mLineOffsetList.size();
    if(actualLine == 0 || line >= actualLine) return std::string();
    actualLine = line;
    
    const LineOffset& lineOffset = mLineOffsetList[actualLine];
    return get(lineOffset.start, lineOffset.end - (withLineEnding ? 0 : 1));
}

size_t Source::length() const
{
    return mSource.length();
}

bool Source::nextAtEnd() const
{
    return nextIndex() >= length();
}

void Source::newline()
{
    mCurrentLine = CurrentLine(line() + 1, 
                            mCurrentLine.endGlobal + 1, 
                            getCurrentLineEndIndex());
    saveCurrentLineOffsets();
}

bool Source::match(char c)
{
    if(nextAtEnd()) return false;
    else if(c != peek()) return false;

    advance();
    return true;
}

void Source::clearSourceOffsets()
{
    mSourceOffsets = SourceOffsets();
}

void Source::clearCurrentLine()
{
    mCurrentLine = CurrentLine();
}

void Source::saveCurretLineEndIndex()
{
    mCurrentLine.endGlobal = getCurrentLineEndIndex();
}

void Source::saveCurrentLineOffsets()
{
    mLineOffsetList.push_back(LineOffset(mCurrentLine.startGlobal, mCurrentLine.endGlobal));
}

size_t Source::getCurrentLineEndIndex() const
{
    size_t found = mSource.find('\n', mCurrentLine.startGlobal);
    if(found == std::string::npos) found = mSource.size();
    return found;
}

void Source::setSourceString(const std::string& str)
{
    mSource = str; 
}
