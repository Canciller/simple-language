#include "simple/simple.h"

using namespace simple;

int main(int argc, char** argv) 
{
    Simple interpreter;
    return interpreter.run(argc, argv);
}
