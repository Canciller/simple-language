#include <map>

#include "simple/scanner.h"

using namespace simple;

typedef std::map<std::string, TokenType> KeywordMap;

KeywordMap createKeywords()
{
    KeywordMap kws;
    kws["and"] = TOKEN_AND;
    kws["class"] = TOKEN_CLASS;
    kws["else"] = TOKEN_ELSE;
    kws["false"] = TOKEN_FALSE;
    kws["function"] = TOKEN_FUNCTION;
    kws["for"] = TOKEN_FOR;
    kws["if"] = TOKEN_IF;
    kws["nil"] = TOKEN_NIL;
    kws["or"] = TOKEN_OR;
    kws["print"] = TOKEN_PRINT;
    kws["return"] = TOKEN_RETURN;
    kws["super"] = TOKEN_SUPER;
    kws["this"] = TOKEN_THIS;
    kws["true"] = TOKEN_TRUE;
    kws["var"] = TOKEN_VAR;
    kws["while"] = TOKEN_WHILE;
    kws["break"] = TOKEN_BREAK;
    kws["continue"] = TOKEN_CONTINUE;
    return kws; 
}

KeywordMap keywords = createKeywords();

void Scanner::syntaxError(size_t line, size_t character, 
                          const std::string& what, 
                          const std::string& where)
{
    mError = true;
    Status::singleton()->report(line, character, "SyntaxError: " + what, where);
}

Scanner::Scanner() : mStartToken(0), mStartTokenRelative(0),
                     mError(false) {}

Scanner::Scanner(const Source& source) : Scanner() 
{
    mSource = source;
}

void Scanner::reset()
{
    init(std::string());
}

void Scanner::init(const std::string& sourceStr)
{
    mSource = Source(sourceStr);
    mStartToken = 0;
    mStartTokenRelative = 0;
    mError = false;
    
    clearTokens();
}

void Scanner::attachSource(Source source)
{
    mSource = source;
}

void Scanner::clearTokens()
{
    mTokens.clear();
}

bool Scanner::hasError() const
{
    return mError;
}

const Source& Scanner::getSource() const
{
    return mSource;
}

const TokenList& Scanner::scanTokens() 
{
    while(!mSource.nextAtEnd()) {
        mStartToken = mSource.nextIndex();
        mStartTokenRelative= mSource.nextIndexRelative(); 
        scanToken();
    }

    mTokens.push_back(Token(TOKEN_EOF, "", "", mSource.line(), mSource.nextIndexRelative()));

    return mTokens;
}

void Scanner::pushToken(TokenType type, const std::string& literal)
{
    std::string lexeme = mSource.get(mStartToken, mSource.currentIndex());
    mTokens.push_back(Token(type, lexeme, literal, mSource.line(), mSource.currentIndexRelative()));
}

void Scanner::scanToken()
{
    char c = mSource.advance();
    switch(c) {
        case '(': pushToken(TOKEN_LEFT_PAREN); break;
        case ')': pushToken(TOKEN_RIGHT_PAREN); break;
        case '{': pushToken(TOKEN_LEFT_BRACE); break;
        case '}': pushToken(TOKEN_RIGHT_BRACE); break;
        case ',': pushToken(TOKEN_COMMA); break;
        case '.':
            if(isDigit(mSource.peek())) number();
            else pushToken(TOKEN_DOT); 
            break;
        case '?': pushToken(TOKEN_QUESTION); break;
        case '-': pushToken(TOKEN_MINUS); break;
        case '+': pushToken(TOKEN_PLUS); break;
        case ';': pushToken(TOKEN_SEMICOLON); break;
        case ':': pushToken(TOKEN_COLON); break;
        case '*': pushToken(TOKEN_STAR); break;
        case '!': pushToken(mSource.match('=') ? TOKEN_BANG_EQUAL : TOKEN_BANG); break;
        case '=': pushToken(mSource.match('=') ? TOKEN_EQUAL_EQUAL : TOKEN_EQUAL); break;
        case '>': pushToken(mSource.match('=') ? TOKEN_GREATER_EQUAL : TOKEN_GREATER); break;
        case '<': pushToken(mSource.match('=') ? TOKEN_LESS_EQUAL : TOKEN_LESS); break;
        case '/': 
            if(mSource.match('/')) {
                while(!mSource.nextAtEnd() && mSource.peek() != '\n') 
                    mSource.advance();
            } 
            else if(mSource.match('*')) comment();
            else pushToken(TOKEN_SLASH);
            break;
        case ' ': case '\t': case '\r': case '\b': case '\v': break;
        case '\n': mSource.newline(); break;
        case '"': string(); break;
        default: 
            if(isDigit(c)) number();
            else if(isAlpha(c)) identifier();
            else {
                syntaxError(mSource.line(), mSource.currentIndexRelative(), "Unexpected character", mSource.getCurrentLine());
            }
            break;
    }
}

void Scanner::string()
{
    char n;
    while(!mSource.nextAtEnd() && (n = mSource.peek()) != '\n' && n != '"') 
        mSource.advance();

    if(mSource.nextAtEnd() || n == '\n') {
        syntaxError(mSource.line(), mSource.currentIndexRelative(), "Unfinished string", mSource.getCurrentLine());
        return;
    }

    mSource.advance();
    pushToken(TOKEN_STRING, mSource.get(mStartToken + 1, mSource.currentIndex() - 1));
}

void Scanner::number()
{
    bool decimal = mSource.current() == '.';
    while(isDigit(mSource.peek()))
        mSource.advance();

    if(!decimal && mSource.match('.')) {
        if(!isDigit(mSource.peek())) {
            syntaxError(mSource.line(), mSource.currentIndexRelative() + 1, "Expected a number", mSource.getCurrentLine());
            return;
        }
        while(isDigit(mSource.peek()))
            mSource.advance();
    }

    pushToken(TOKEN_NUMBER, mSource.get(mStartToken, mSource.currentIndex()));
}

bool isLiteral(TokenType type)
{
    return type == TOKEN_TRUE   ||
           type == TOKEN_FALSE  ||
           type == TOKEN_NIL;
}

void Scanner::identifier()
{
    while(isAlphaNumeric(mSource.peek())) mSource.advance();
    TokenType type = TOKEN_IDENTIFIER;
    std::string lexeme = mSource.get(mStartToken, mSource.currentIndex());

    auto keyword = keywords.find(lexeme);
    if(keyword != keywords.end()) 
        type = keyword->second;

    std::string literal;
    if(isLiteral(type)) literal = lexeme;

    pushToken(type, literal);
} 

void Scanner::comment() 
{
    size_t line = mSource.line(),
            character = mSource.currentIndexRelative();
    std::string commentLine = mSource.getCurrentLine();

    bool commentClosed = false;
    while(!mSource.nextAtEnd() && !commentClosed) {
        char c = mSource.advance();
        switch(c) {
            case '*':
                if(mSource.match('/'))
                    commentClosed = true;
                break;
            case '/':
                if(mSource.match('*')) 
                    comment();
                break;
            case '\n': mSource.newline(); break;
            // default: ignore everthing
        }
    }
    if(!commentClosed) { 
        syntaxError(line, character, "Unfinished block comment", commentLine);
    }
}

bool Scanner::isAlphaNumeric(char c)
{
    return isAlpha(c) || isDigit(c);
}

bool Scanner::isAlpha(char c) 
{
    return (c >= 'A' && c <= 'Z') ||
            (c >= 'a' && c <= 'z');
}

bool Scanner::isDigit(char c) 
{
    return c >= '0' && c <= '9';
}
