#!/bin/python

types = {
    'expr': {
        'Binary' : ['Expr::SharedPtr left', 'Token op', 'Expr::SharedPtr right'],
        'Grouping' : ['Expr::SharedPtr ex'],
        'Literal' : ['Any value'],
        'Uninary' : ['Token op', 'Expr::SharedPtr right'],
    },
    'stmt': {
        'Expression' : ['expr::Expr::SharedPtr ex'],
        'Print' : ['expr::Expr::SharedPtr ex'] 
    }
}

tab = '    '

def addToNamespace(namespace, content):
    return '''
namespace {0} 
{{
{1}
}}
'''.format(namespace, content);

def defineAst(types):
    header = '''#ifndef SIMPLE_EXPR_H
#define SIMPLE_EXPR_H

#include <memory>

#include "simple/token.h"
#include "simple/any.h"
'''
    final = ''
    for ns, T in types.items():
        baseClass = ns.capitalize();
        s = defineBase(baseClass)
        for className, fields in T.items():
            s += defineDerived(className, baseClass, fields);
        s = addToNamespace(ns, s)
        final += s
    final = defineVisitor(types) + final
    final = addToNamespace('simple', final) + '#endif'
    final = header + final
    print(final)
        

def defineBase(className):
    return '''
class {0} 
{{
public:
    using SharedPtr = std::shared_ptr<{0}>;

    virtual ~{0}() = default;
    
    virtual Any accept(Visitor& visitor) = 0;
}};
'''.format(className)

def defineDerived(className, baseClass, fields):
    n = len(fields)
    paramList = ''
    construtorList = ''
    dataMembers = ''
    for i in range(n):
        paramName = fields[i].split()[1]
        paramList += fields[i]
        construtorList += '{0}({0})'.format(paramName)
        dataMembers += tab + fields[i] + ';'
        if i != n - 1: 
            paramList += ', '
            construtorList += ', '
            dataMembers += '\n'
    return '''
class {0} : public {1}
{{
public:
    {0}({2}) : {3} {{}}
    
    Any accept(Visitor& visitor) override
    {{
        return visitor.visit(*this);
    }}
    
{4}
}};
'''.format(className, baseClass, paramList, construtorList, dataMembers)

def defineVisitor(types):
    namespace = ''
    visitor = '''
class Visitor
{
public:
    virtual ~Visitor() = default;'''
    for ns, T in types.items():
        namespace += '''
namespace {0}
{{'''.format(ns)
        for className, v in T.items():
            namespace += '''
    class {0};'''.format(className)
            visitor += '''
    virtual Any visit({0}::{1}&) = 0;'''.format(ns, className)
        namespace += '\n}\n'
    visitor += '\n};\n'
    visitor = namespace + visitor
    return visitor
            

defineAst(types)
