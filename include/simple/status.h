#ifndef STATUS_H
#define STATUS_H

#include <cstdio>
#include <iostream>
#include <string>
#include <sstream>

#include "simple/source.h"
#include "simple/token.h"

namespace simple
{

class Status
{
public:
    Status(const Status&) = delete;

    Status(Status&&) = delete;

    ~Status();
    
    static Status* singleton();

    static void destroy();
    
    void attachSource(const Source* source = nullptr);

    void report(size_t line, size_t character, 
                const std::string& message,
                const std::string& where = std::string()) const;

    void report(const Token& token, 
                const std::string& message, 
                const std::string& where) const;

    void report(const Token& token,
                const std::string& message) const;

    void report(const std::string& message);

 private:
    Status();

    std::string createWhereMessage(size_t line, size_t character, const std::string& where) const;

    std::string getWhereMessageFromSource(size_t line) const;

private:
    static Status* mInstance;
    const Source* mSource; // Status is not owner of this pointer.
};

}
#endif
