#ifndef SIMPLE_VISITOR_H
#define SIMPLE_VISITOR_H

#include "simple/expr.h"
#include "simple/stmt.h"

#define VISIT_EXPR(T)\
virtual Any visit(expr::T&) {\
    return Any();\
}

#define VISIT_STMT(T)\
virtual void visit(stmt::T&) {}

namespace simple 
{
    class Visitor
    {
    public:
        virtual ~Visitor() = default;

        VISIT_EXPR(Assign)
        VISIT_EXPR(Binary)
        VISIT_EXPR(Grouping)
        VISIT_EXPR(Uninary)
        VISIT_EXPR(Literal)
        VISIT_EXPR(Variable)
        VISIT_EXPR(Logical)
        VISIT_EXPR(Conditional)
        VISIT_EXPR(Call)
    
        VISIT_STMT(Block)
        VISIT_STMT(Expression)
        VISIT_STMT(Print)
        VISIT_STMT(Var)
        VISIT_STMT(If)
        VISIT_STMT(While)
        VISIT_STMT(Keyword)
        VISIT_STMT(Function)
    };
}

#undef VISIT_EXPR
#undef VISIT_STMT
#endif
