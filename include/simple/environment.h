#ifndef SIMPLE_ENVIRONMENT_H
#define SIMPLE_ENVIRONMENT_H

#include <unordered_map>
#include <string>

#include "simple/any.h"
#include "simple/error.h"

namespace simple 
{
    class Enviroment
    {
    public:
        Enviroment() : mEnclosing(nullptr) {}

        Enviroment(Enviroment* enclosing) : mEnclosing(enclosing) {}

        ~Enviroment() 
        {
            mEnclosing = nullptr;
        }

        inline void define(const std::string& name, Any value)
        {
            mValues[name] = value;
        }

        void assign(const Token& name, Any value)
        {
            const std::string& lexeme = name.lexeme();

            auto found = mValues.find(lexeme);
            if(found != mValues.end()) {
                mValues[lexeme] = value;
                return;
            }

            if(mEnclosing) return mEnclosing->assign(name, value);

            throw RuntimeError(name, "Undefined variable '" + lexeme + "'");
        }
        
        Any get(const Token& name)
        {
            const std::string& lexeme = name.lexeme();

            auto value = mValues.find(lexeme);
            if(value != mValues.end()) {
                //if(!value->second) throw RuntimeError(name, "Variable '" + lexeme + "' hasn't been assigned or initialized");
                return value->second;
            }
                            
            if(mEnclosing) return mEnclosing->get(name);
        
            throw RuntimeError(name, "Undefined variable '" + lexeme + "'");
        }

    private:
        std::unordered_map<std::string, Any> mValues;
        Enviroment* mEnclosing; // Parent Enviroment.
    };
}

#endif
