#ifndef SIMPLE_EXPR_H
#define SIMPLE_EXPR_H

#include <memory>

#include "simple/token.h"

#define ACCEPT_VISITOR() Any accept(Visitor& visitor) override;
#define EXPR_SHARED_PTR(T) using SharedPtr = std::shared_ptr<T>;

namespace simple 
{
    class Visitor;

    namespace expr 
    {
        class Expr 
        {
        public:
            using SharedPtr = std::shared_ptr<Expr>;

            virtual ~Expr() = default;
            
            virtual Any accept(Visitor& visitor) = 0;
        };

        using ExprList = std::vector<Expr::SharedPtr>;

        class Assign : public Expr
        { public:
            Assign(const Token& name, Expr::SharedPtr value);
            
            ACCEPT_VISITOR()

            Token name;
            Expr::SharedPtr value;
        };
        
        class Binary : public Expr
        {
        public:
            Binary(Expr::SharedPtr left, const Token& op, Expr::SharedPtr right);
            
            EXPR_SHARED_PTR(Binary)

            ACCEPT_VISITOR()
            
            Expr::SharedPtr left;
            Token op;
            Expr::SharedPtr right;
        };

        class Grouping : public Expr
        {
        public:
            Grouping(Expr::SharedPtr ex);
            
            ACCEPT_VISITOR()

            Expr::SharedPtr ex;
        };

        class Literal : public Expr
        {
        public:
            Literal(const Any& value);
           
            ACCEPT_VISITOR()
            
            Any value;
        };

        class Uninary : public Expr
        {
        public:
            Uninary(const Token& op, Expr::SharedPtr right);
            
            ACCEPT_VISITOR()
            
            Token op;
            Expr::SharedPtr right;
        };

        class Variable : public Expr
        {
        public:
            Variable(const Token& name);

            ACCEPT_VISITOR()

            Token name;
        };

        class Logical : public Expr
        {
        public:
            Logical(Expr::SharedPtr left, const Token& op, Expr::SharedPtr right);

            ACCEPT_VISITOR()

            Expr::SharedPtr left;
            Token op;
            Expr::SharedPtr right;
        };

        class Conditional : public Expr
        {
        public:
            Conditional(Expr::SharedPtr condition, Expr::SharedPtr left, Expr::SharedPtr right);

            ACCEPT_VISITOR()

            Expr::SharedPtr condition, left, right;
        };

        class Call : public Expr
        {
        public:
            Call(Expr::SharedPtr callee, const Token& paren, const ExprList& arguments);

            ACCEPT_VISITOR()

            Expr::SharedPtr callee;
            Token paren;
            ExprList arguments;
        };
    }

    using ExprSharedPtr = expr::Expr::SharedPtr;
}

#undef ACCEPT_VISITOR
#undef EXPR_SHARED_PTR
#endif
