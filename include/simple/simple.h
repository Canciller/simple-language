#include <fstream>
#include <iostream>
#include <string>

#include "simple/interpreter.h"
#include "simple/scanner.h"
#include "simple/parser.h"

namespace simple 
{

class Simple 
{
public:
    Simple();

    ~Simple();

    int run(int argc, char **argv);

    int exitCode() const;

private:
    int runFile(const std::string& path);
      
    void runPrompt();

    int process(const std::string& source); 

private:
    Status* mStatus;
    Scanner* mScanner;
    Parser* mParser;
    Interpreter* mInterpreter;
    int mExitCode;
};

}
