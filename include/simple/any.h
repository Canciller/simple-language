#ifndef SIMPLE_ANY_H
#define SIMPLE_ANY_H

#include <stdexcept>
#include <typeinfo>
#include <sstream>
#include <string>
#include <memory>

namespace simple 
{
    class BadAnyCast : std::bad_cast
    {
        virtual const char* what()
        {
            return "simple::BadAnyCast"
                   "failed conversion using simple::AnyCast";
        }
    };

    class Any
    { 
    private:
        class Concept 
        {
        public:
            Concept() {}
            virtual ~Concept() {}
            virtual std::string toString() const = 0;
            virtual const std::type_info& type() const = 0; 
        };

        template<typename T>
        class Model : public Concept
        {
        public:
            Model(T value) : value(value) {}
            std::string toString() const override 
            {
                std::stringstream ss;
                ss << value;
                return ss.str();
            }

            const std::type_info& type() const override 
            {       
                return typeid(T);
            }   

            T value;
        };

    public:
        template<typename T>
        friend T AnyCast(Any& a);

        const std::type_info&  type() const
        {
            return self->type();
        }

        std::string toString() const
        {
            if(empty()) return std::string();
            return self->toString();
        }

        bool empty() const
        {
            return !self;
        }

        template<typename T>
        T castTo() const
        {
            if(type() != typeid(T))
                throw BadAnyCast();
            Model<T>* model = static_cast<Model<T>*>(self.get()); 
            return model->value;
        }

        template<typename T>
        bool isType() const
        {
            if(empty()) return false;
            return type() == typeid(T);
        }

        friend void swap(Any& first, Any& second)
        {
            using std::swap;
            swap(first.self, second.self);
        }

        template<typename T>
        Any(T value) : self(std::make_shared<Model<T>>(value)) {}
        
        Any(const Any& other)
        {
            self = other.self;
        }

        Any(Any&& other) : Any()
        {
            swap(*this, other);
        }

        Any() {}

        Any& operator=(Any other)
        {
            swap(*this, other);
            return *this;
        }

        operator bool()
        {
            return !empty();
        }

    private:
        std::shared_ptr<Concept> self;
    };
    
    template<typename T>
    T AnyCast(Any& a) {
        if(a.type() != typeid(T))
            throw BadAnyCast();
        Any::Model<T>* model = static_cast<Any::Model<T>*>(a.self.get()); 
        return model->value;
    }

    template<>
    class Any::Model<bool> : public Any::Concept
    {
    public:
        Model(bool value) : value(value) {}

        std::string toString() const override 
        {
            std::stringstream ss;
            ss << (value ? "true" : "false");
            return ss.str();
        }

        const std::type_info& type() const override 
        {       
            return typeid(bool);
        }   

        bool value;
    };
}

#endif
