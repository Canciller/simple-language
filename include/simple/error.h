#ifndef SIMPLE_ERROR_H
#define SIMPLE_ERROR_H

#include <stdexcept>

#include "simple/token.h"

namespace simple 
{
    class Error : public std::runtime_error
    {
    public:
        Error(const Token& token, const std::string& what);
        Token token() const; 
    private:
        Token mToken;
    };
    
    using RuntimeError = Error;
    using ParseError = Error;
}

#endif
