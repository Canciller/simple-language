#ifndef SIMPLE_FLAGS_H
#define SIMPLE_FLAGS_H

namespace simple
{
    class Flags
    {
    public:
        static Flags* singleton()
        {
            if(!mInstance)
                mInstance = new Flags();
            return mInstance;
        }

        static void destroy()
        {
            if(mInstance) delete mInstance;
            mInstance = nullptr;
        }

    public:
        bool interactive; 

    private:
        Flags() : interactive(false) {}

    private:
        static Flags* mInstance;
    };
}

#endif
