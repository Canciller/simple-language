#ifndef SIMPLE_STMT_H
#define SIMPLE_STMT_H

#include <memory>

#include "simple/expr.h"

#define ACCEPT_VISITOR() void accept(Visitor& visitor) override;

namespace simple 
{
    class Visitor;

    namespace stmt
    {
        class Stmt;
    }

    using StmtSharedPtr = std::shared_ptr<stmt::Stmt>;
    using StmtList = std::vector<StmtSharedPtr>;

    namespace stmt 
    {
        class Stmt 
        {
        public:
            using SharedPtr = StmtSharedPtr;

            virtual ~Stmt() = default;
            
            virtual void accept(Visitor& visitor) = 0;
        };
        
        class Block : public Stmt
        {
        public:
            Block(const StmtList& statements);

            ACCEPT_VISITOR()

            StmtList statements;
        };

        class Expression : public Stmt
        {
        public:
            Expression(ExprSharedPtr ex);
            
            ACCEPT_VISITOR()

            ExprSharedPtr ex;
        };

        class Print : public Stmt
        {
        public:
            Print(ExprSharedPtr ex);
            
            ACCEPT_VISITOR()
            
            ExprSharedPtr ex;
        };
        
        class Var : public Stmt
        {
        public:
            Var(const Token& name, ExprSharedPtr initializer);

            ACCEPT_VISITOR()
            
            Token name;
            ExprSharedPtr initializer;
        };

        class If : public Stmt
        {
        public:
            If(ExprSharedPtr condition, StmtSharedPtr ifBranch, StmtSharedPtr elseBranch);
            
            ACCEPT_VISITOR()

            ExprSharedPtr condition;
            StmtSharedPtr ifBranch;
            StmtSharedPtr elseBranch;
        };

        class While : public Stmt
        {
        public:
            While(ExprSharedPtr condition, StmtSharedPtr body);

            ACCEPT_VISITOR()

            ExprSharedPtr condition;
            StmtSharedPtr body;
        };

        class Keyword : public Stmt
        {
        public:
            Keyword(const Token& keyword);
            
            ACCEPT_VISITOR()

            Token keyword;
        };

        class Function : public Stmt
        {
        public:
            Function(const Token& name, const TokenList& params, const StmtList& body);

            ACCEPT_VISITOR()

            Token name;
            TokenList params;
            StmtList body;
        };
    }
}

#undef ACCEPT_VISITOR
#endif
