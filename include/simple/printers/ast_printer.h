#ifndef AST_PRINTER_H
#define AST_PRINTER_H

#include <list>

#include "simple/visitor.h"

namespace simple
{
    class ASTPrinter : public Visitor 
    {
    public:
        virtual ~ASTPrinter() = default;

        std::string print(expr::Expr& ex) 
        {
            Any tree = ex.accept(*this);
            if(!tree || tree.type() != typeid(std::string))
                return std::string();
            return AnyCast<std::string>(tree);
        }
        
        Any visit(expr::Binary& ex) override 
        { 
            return parenthesize(ex.op.lexeme(), { ex.left.get(), ex.right.get() });     
        }
        
        Any visit(expr::Grouping& ex) override 
        {
            return parenthesize("group", { ex.ex.get() }); 
        }
        
        Any visit(expr::Literal& ex) override 
        {
            if(ex.value.empty()) return std::string("nil");
            if(ex.value.isType<std::string>()) 
                return "\"" + ex.value.toString() + "\"";
            return ex.value.toString();
        }
        
        Any visit(expr::Uninary& ex) override 
        {
            return parenthesize(ex.op.lexeme(), { ex.right.get() }); 
        }
        
        Any visit(expr::Variable& ex) override
        {
            return parenthesize("var " +ex.name.lexeme());
        }

        Any visit(expr::Logical& ex) override
        {
            return parenthesize(ex.op.lexeme(), { ex.left.get(), ex.right.get() });     
        }

        Any visit(expr::Assign& ex) override
        {
            return parenthesize(ex.name.lexeme() + " =", {ex.value.get() });
        }

        Any visit(expr::Conditional& ex) override
        {
            const std::string &condition = ex.condition->accept(*this).toString(),
                              &left = ex.left->accept(*this).toString(),
                              &right = ex.right->accept(*this).toString(); 
            return "(" + condition + " ? " + left + " : " + right + ")";
        }

        Any visit(expr::Call& ex) override
        {   
            std::stringstream ss;
            ss << ex.callee->accept(*this).toString() << "("; 
            for(auto& arg : ex.arguments)
                ss << arg->accept(*this).toString() << ", ";
            ss << ")";
            return ss.str();
        }

    private:
        std::string parenthesize(const std::string& name, std::list<expr::Expr*> exprs = {})
        {
            std::stringstream ss;
            ss << "(" << name;
            for(expr::Expr* ex : exprs)
                ss << " " << ex->accept(*this).castTo<std::string>(); 
            ss << ")";
            return ss.str();
        }
    };
}

#endif
