#ifndef RPN_PRINTER_H
#define RPN_PRINTER_H

#include <sstream>
#include <vector>

#include "simple/visitor.h"

namespace simple
{
    class RPNPrinter : public Visitor 
    {
    public:
        virtual ~RPNPrinter() = default;

        std::string print(expr::Expr& ex) 
        {
            Any tree = ex.accept(*this);
            if(tree.type() != typeid(std::string))
                return std::string();
            return AnyCast<std::string>(tree);
        }

        Any visit(expr::Binary& ex) override 
        { 
            return push(ex.op.lexeme(), { ex.left.get(), ex.right.get() });     
        }
        
        Any visit(expr::Grouping& ex) override 
        {
            return push("", { ex.ex.get() }); 
        }
        
        Any visit(expr::Literal& ex) override 
        {
            if(ex.value.empty()) return std::string("nil");
            if(ex.value.isType<std::string>()) 
                return "\"" + ex.value.toString() + "\"";
            return ex.value.toString();
        }
        
        Any visit(expr::Uninary& ex) override 
        {
            return push(ex.op.lexeme(), { ex.right.get() }); 
        }

    private:
        std::string push(const std::string& name, std::vector<expr::Expr*> exprs)
        {
            std::stringstream ss;
            size_t n = exprs.size();
            for(size_t i = 0; i != n; ++i) {
                ss << exprs[i]->accept(*this).castTo<std::string>();
                if(i != n - 1) ss << ' ';
            }
            if(!name.empty()) ss << ' ' << name;
            return ss.str();
        }
    };
}

#endif
