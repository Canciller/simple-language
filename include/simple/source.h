#ifndef SOURCE_H
#define SOURCE_H

#include <vector>
#include <iostream>
#include <string>

namespace simple
{

class Source {
public:
    Source();
    
    Source(const Source& other);

    Source(Source&& other);

    Source(const std::string& source);
     
    ~Source() = default;
    
    Source& operator=(Source other);

    friend void swap(Source& first, Source& second);

    void load(const std::string& source);
    
    void reset();
 
    // returns the current character been analized and advances the line offsets.
    char advance();
    
    size_t nextIndex() const;

    size_t currentIndex() const;

    size_t currentIndexRelative() const;

    size_t nextIndexRelative() const;

    size_t line() const;

    // returns the the next character after current without advancing the line offsets.
    char peek() const;

    // returns the current character without advancing the line offsets.
    char current() const;

    // returns substring of lines from index start to index end.
    std::string get(size_t start = 0, size_t end = std::string::npos) const;
    
    std::string getCurrentLine(bool withLineEnding = false) const;

    std::string getLine(size_t line, bool withLineEnding = false) const;

    size_t length() const;

    bool nextAtEnd() const;

    void newline();

    bool match(char c);

private:
    void clearSourceOffsets();

    void clearCurrentLine();

    void saveCurretLineEndIndex();

    void saveCurrentLineOffsets();

    size_t getCurrentLineEndIndex() const;

    inline void setSourceString(const std::string& str);

private:
    std::string mSource; 

    struct LineOffset {
        LineOffset(size_t start_, size_t end_) : 
            start(start_), end(end_) {}
        size_t start;
        size_t end;
    };

    struct SourceOffsets {
        SourceOffsets() :
            current(0),
            next(0) {}

        size_t current;
        size_t next;
    } mSourceOffsets;

    struct CurrentLine {
        CurrentLine(size_t lineNo = 0, 
                    size_t startG = 0,
                    size_t endG = 0) :
            lineNumber(lineNo), 
            startGlobal(startG),
            currentRelative(0),
            nextRelative(0),
            endGlobal(endG) {}

        size_t lineNumber;
        size_t startGlobal;
        size_t currentRelative;
        size_t nextRelative;
        size_t endGlobal;
    } mCurrentLine;

    std::vector<LineOffset> mLineOffsetList;
};

void swap(Source&, Source&);

}

#endif
