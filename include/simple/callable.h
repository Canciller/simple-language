#ifndef SIMPLE_CALLABLE_H
#define SIMPLE_CALLABLE_H

#include <iostream>
#include <vector>

#include "simple/any.h"

namespace simple
{
    class Interpreter;

    class Callable
    {
    public:
        using ArgList = std::vector<Any>;

        virtual ~Callable() = default;

        virtual size_t ariaty() = 0;
        
        virtual Any call(Interpreter& interpreter, ArgList& arguments) = 0;

        virtual Any operator()(Interpreter& interpreter, ArgList& arguments)
        {
            return call(interpreter, arguments);
        }
        
        virtual std::string toString() const
        {
            return "<native function>";
        }

        friend std::ostream& operator<< (std::ostream& stream, const Callable& callable);
    };

    using CallableSharedPtr = std::shared_ptr<Callable>;

    class Greet : public Callable
    {
    public:
        size_t ariaty() 
        {
            return 1;
        }

        Any call(Interpreter& interpreter, ArgList& arguments)
        {
            std::cout << "Hello, " << arguments[0].toString() << '\n';
            return Any(); 
        }
    };
}

std::ostream& operator<< (std::ostream& stream, const simple::Callable& callable)
{
    stream << callable.toString();
    return stream;
}

#endif
