#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <vector>
#include <typeinfo>
#include <sstream>
#include <cmath>

#include "simple/environment.h"
#include "simple/visitor.h"
#include "simple/status.h"
#include "simple/error.h"

#define TYPE_BOOL(a) a.isType<bool>()
#define TO_BOOL(a) a.castTo<bool>()

#define TYPE_STR(a) a.isType<std::string>()
#define TO_STR(a) a.castTo<std::string>()

#define TYPE_NUM(a) a.isType<double>()
#define TO_NUM(a) a.castTo<double>()

#define TYPE_NIL(a) a.empty()

namespace simple
{
    class Interpreter : public Visitor 
    {
    public: 
        Interpreter();
        
        Interpreter(const Interpreter&) = delete;

        Interpreter(Interpreter&&) = delete;

        Interpreter& operator=(Interpreter other) = delete;

        ~Interpreter();

        void reset();

        void interpret(StmtList& statements);

        bool hasError() const;

        Enviroment* globals();
    
        // Expressions

        Any visit(expr::Assign& expr) override;

        Any visit(expr::Binary& expr) override;

        Any visit(expr::Grouping& expr) override;

        Any visit(expr::Uninary& expr) override;

        Any visit(expr::Literal& expr) override;
        
        Any visit(expr::Variable& expr) override;

        Any visit(expr::Logical& expr) override;

        Any visit(expr::Conditional& expr) override;

        Any visit(expr::Call& expr) override;

        // Statements

        void visit(stmt::Block& stmt) override;

        void visit(stmt::Expression& stmt) override;

        void visit(stmt::Print& stmt) override;
        
        void visit(stmt::Var& stmt) override;

        void visit(stmt::If& stmt) override;

        void visit(stmt::While& stmt) override;

        void visit(stmt::Keyword& stmt) override;

        void visit(stmt::Function& stmt) override;


        void execute(StmtSharedPtr& stmt);
        
        void executeBlock(StmtList& statements, Enviroment* environment);

        Any evaluate(expr::Expr::SharedPtr expr);

    private:
        void runtimeError(RuntimeError& e);

        bool isTruthy(const Any& value) const;

        bool isEqual(const Any& a, const Any& b) const;

        void checkStringOperands(const Token& op, const Any& a, const Any& b);

        void checkNumberOperands(const Token& op, const Any& a, const Any& b);

    private:
        Enviroment* mEnvironment;
        Enviroment* mGlobal;
        bool mError;
    };
}

#endif
