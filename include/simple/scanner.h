#ifndef SCANNER_H
#define SCANNER_H

#include <functional>
#include <vector>
#include <string>

#include "simple/status.h"
#include "simple/source.h"
#include "simple/token.h"

namespace simple 
{

class Scanner
{
public:
    Scanner();

    Scanner(const Source& source);

    ~Scanner() = default;
    
    void reset(); // init with empty source

    void init(const std::string& sourceStr);

    void attachSource(Source source);
 
    void clearTokens();

    bool hasError() const;

    const Source& getSource() const;

    const TokenList& scanTokens();
    
private:
    void syntaxError(size_t line, size_t character, 
                     const std::string& what, 
                     const std::string& where = std::string());

    void pushToken(TokenType type, const std::string& literal = std::string());

    void scanToken();
    
    void string();

    void number();

    void identifier();

    void comment();

    bool isAlphaNumeric(char c);

    bool isAlpha(char c);

    bool isDigit(char c);

private:
    Source mSource;
    TokenList mTokens;
    size_t mStartToken;
    size_t mStartTokenRelative;
    bool mError;
};

}
#endif
