#ifndef SIMPLE_PARSER_H
#define SIMPLE_PARSER_H

#include <memory>
#include <vector>
#include <functional>
#include <initializer_list>

#include "simple/status.h"
#include "simple/expr.h"
#include "simple/stmt.h"
#include "simple/error.h"

namespace simple 
{
    class Parser 
    {
    public:
        Parser();

        Parser(TokenList tokens); 
        
        Parser(const Parser& other);

        Parser(Parser&& tokens);
        
        ~Parser() = default;
 
        Parser& operator=(Parser other);

        friend void swap(Parser& first, Parser& second);
        
        void reset(); // init with empty token list

        void init(TokenList tokens);

        StmtList parse();
        
        bool hasError() const;

    private:
        // Statements

        StmtSharedPtr declaration();

        StmtSharedPtr varDeclaration();

        StmtSharedPtr statement();
        
        StmtList block();

        StmtSharedPtr printStatement();

        StmtSharedPtr expressionStatement();

        StmtSharedPtr ifStatement();

        StmtSharedPtr whileStatement();

        StmtSharedPtr forStatement();

        StmtSharedPtr function(const std::string& type);

        // Expressions

        ExprSharedPtr expression();
        
        ExprSharedPtr assigment();
        
        ExprSharedPtr conditional();

        ExprSharedPtr equality();
        
        ExprSharedPtr logicalOr();

        ExprSharedPtr logicalAnd();

        ExprSharedPtr comparison();

        ExprSharedPtr addition();

        ExprSharedPtr multiplication();

        ExprSharedPtr uninary();

        ExprSharedPtr call();

        ExprSharedPtr finishCall(ExprSharedPtr callee);

        ExprSharedPtr primary();

        void parseError(const ParseError& e);

        const Token& consume(TokenType type, const std::string& message);

        bool match(const std::initializer_list<TokenType>& types);

        bool check(TokenType type);
        
        Token& advance();

        Token& peek();
        
        Token& previous();

        bool isAtEnd();

        void synchronize();

    private:
        TokenList mTokens;
        size_t mCurrent;
        bool mError;
    };
    
    void swap(Parser& first, Parser& second);
}

#endif
