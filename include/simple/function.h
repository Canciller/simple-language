#ifndef SIMPLE_FUNCTION_H
#define SIMPLE_FUNCTION_H

#include "simple/callable.h"
#include "simple/interpreter.h"

namespace simple 
{
    class Function : public Callable 
    {  
    public:
        Function(stmt::Function& declaration) : declaration(declaration) {}
        
        size_t ariaty() override
        {
            return declaration.params.size(); 
        }

        Any call(Interpreter& interpreter, ArgList& arguments) override
        {
            Enviroment env = Enviroment(interpreter.globals());
            for(size_t i = 0; i < ariaty(); ++i) 
                env.define(declaration.params[i].lexeme(), arguments[i]);
            interpreter.executeBlock(declaration.body, &env);
            return Any();
        }

        std::string toString() const override
        {
            return "<fun " + declaration.name.lexeme() + ">";
        }

    private:
        stmt::Function& declaration;
    };
}
#endif
