#ifndef TOKEN_H
#define TOKEN_H

#include <vector>
#include <iostream>
#include <sstream>

#include "simple/any.h"

namespace simple 
{

enum TokenType
{
    TOKEN_LEFT_PAREN, TOKEN_RIGHT_PAREN, TOKEN_LEFT_BRACE, TOKEN_RIGHT_BRACE,
    TOKEN_COMMA, TOKEN_DOT, TOKEN_MINUS, TOKEN_PLUS, TOKEN_SEMICOLON, TOKEN_COLON, TOKEN_SLASH, TOKEN_STAR,
    TOKEN_QUESTION,     

    TOKEN_BANG, TOKEN_BANG_EQUAL,
    TOKEN_EQUAL, TOKEN_EQUAL_EQUAL,
    TOKEN_GREATER, TOKEN_GREATER_EQUAL,
    TOKEN_LESS, TOKEN_LESS_EQUAL,

    TOKEN_IDENTIFIER, TOKEN_STRING, TOKEN_NUMBER,

    TOKEN_AND, TOKEN_CLASS, TOKEN_ELSE, TOKEN_FALSE, TOKEN_FUNCTION, TOKEN_FOR, TOKEN_IF, TOKEN_NIL, TOKEN_OR,
    TOKEN_PRINT, TOKEN_RETURN, TOKEN_SUPER, TOKEN_THIS, TOKEN_TRUE, TOKEN_VAR, TOKEN_WHILE, 
    
    TOKEN_BREAK, TOKEN_CONTINUE,

    TOKEN_EOF, 
    TOKEN_UNKNOWN // This is not a token.
};

class Token
{
public:
    Token();
    
    Token(TokenType type,
          size_t line,
          size_t character);

    Token(TokenType type, 
          std::string lexeme, 
          std::string literal, 
          size_t line,
          size_t character);

    virtual ~Token() = default;

    TokenType type() const;

    std::string lexeme() const;

    std::string literal() const;
    
    const Any& value() const;

    size_t line() const;

    size_t character() const;

    std::string toString() const;
    
    const char* typeString() const;

protected:
    TokenType mType;
    std::string mLexeme;
    Any mValue;
    size_t mLine;
    size_t mCharacter;
};

using TokenList = std::vector<Token>;

}
#endif
