#include "simple/source.h"
#include "simple/any.h"
#include <iostream>

using namespace simple;

std::string generateLines(size_t lines)
{
    std::string text;
    for(size_t i = 0; i < lines; ++i) 
        text += std::string("this is the line no. ") + std::to_string(i + 1) + "\n";
    return text;
}

int main() 
{
    size_t lines = 50;
    Source source(generateLines(lines));
    while(!source.nextAtEnd()) {
        char c = source.advance();
        if(c == '\n')
            source.newline();
    }
    
    for(size_t i = 0; i < lines; ++i) 
        std::cout << source.getLine(i, true);
    
    Any a(true);
    std::cout << a.toString() << '\n'; 
    Any b(2.0);
    std::cout << b.toString() << '\n'; 
    Any c(std::string("test"));
    std::cout << c.castTo<std::string>() << '\n'; 

    return EXIT_SUCCESS;
}
